import React from "react";

import RemoteTerminal from "@/components/remote_terminal";

export default function HomePageIndex(_props) {
  
  return (
    <div className="home-index-container">
      <section>
        <h1>Terminally Online, for Holistics</h1>
      </section>
      <RemoteTerminal />
      <section className="made-by">
        Made by Ban
      </section>
    </div>
  );
};
