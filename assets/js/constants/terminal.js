export const USER = 'unknown';

export const HOST_NAME = 'RemoteTerminal';

export const VALID_COMMANDS = ['cd', 'cr', 'cat', 'ls', 'find', 'up', 'mv', 'rm', 'man'];

export const DEFAULT_GREETING =
  `Welcome to Remote Terminal.
  To check the manual for a particular command, use man COMMAND_NAME
  List of available commands: cd cr cat ls find up mv man clear
  `;
