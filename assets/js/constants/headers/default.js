export const CSRF_TOKEN = document.querySelector("meta[name='csrf-token']").content;

export const DEFAULT_HEADERS = {
  'Accept': 'application/json',
  'Content-Type': 'application/json',
  'X-CSRF-Token': CSRF_TOKEN
};