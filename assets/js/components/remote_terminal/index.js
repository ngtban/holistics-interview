import React, { useState } from "react";
import Terminal from "react-console-emulator";

import { 
  DEFAULT_GREETING, USER, HOST_NAME, VALID_COMMANDS
} from "@/constants/terminal";
import RemoteTerminalApi from "./api";

export default function RemoteTerminal(_props) {
  const [directory, setDirectory] = useState({ name: null, parent: null });

  let currentPath = null;

  if (directory.parent === "") {
    currentPath = `/${directory.name}`
  } else if (directory.parent) {
    currentPath = `${directory.parent}/${directory.name}`;
  } else {
    currentPath = `/`;
  }

  const promptLabel = `${USER}@${HOST_NAME}:${currentPath}`;

  const list_commands = VALID_COMMANDS.reduce((accumulator, command) => {
    accumulator[command] = {
        fn: async function(...args) {
          const response =
            await RemoteTerminalApi.executeCommand({
              directory,
              command: `${command} ${args.join(' ')}`
            });

          const body = await response.json();

          setDirectory(body.directory);

          return body.output;
        }
      }

    return accumulator;
  }, {});

  const with_pwd_list_commands =
    { 
      ...list_commands,
      pwd: {
        fn: function() {
          return `${currentPath}`;
        }
      }
    };

  return (
      <div className="terminal-container">
        <Terminal
          className="terminal"
          commands={with_pwd_list_commands}
          welcomeMessage={DEFAULT_GREETING}
          promptLabel={promptLabel}
        />
      </div>
  );
};
