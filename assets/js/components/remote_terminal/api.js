import { DEFAULT_HEADERS } from "@/constants/headers/default";

const RemoteTerminalApi = {
  executeCommand({ directory, command }) {
    return fetch(
      'execute',
      {
        method: 'POST',
        headers: DEFAULT_HEADERS,
        body: JSON.stringify({ directory, command })
      }
    );
  }
};

export default RemoteTerminalApi;