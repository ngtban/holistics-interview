# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :remote_terminal,
  ecto_repos: [RemoteTerminal.Repo]

# Configures the endpoint
config :remote_terminal, RemoteTerminalWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "vfPJvkCtUAn790yJG4qRhbgtFRYDFBkLSL9oMoKqKm5y68MMIQZn2JHCI5fu9UeD",
  render_errors: [view: RemoteTerminalWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: RemoteTerminal.PubSub,
  live_view: [signing_salt: "zPkEQpaR"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
