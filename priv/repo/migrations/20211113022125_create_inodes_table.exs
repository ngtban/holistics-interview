defmodule RemoteTerminal.Repo.Migrations.CreateInodesTable do
  use Ecto.Migration

  def up do
    create table("inodes") do
      add :parent, :text
      add :name, :string, size: 256
      add :type, :string, size: 256, null: false
      add :data, :text

      timestamps(default: fragment("now()"))
    end

    create unique_index("inodes", [:parent, :name])
  end

  def down do
    drop table("inodes")
  end
end
