defmodule RemoteTerminal.Repo.Migrations.AddTrigramIndexOnInodeParents do
  use Ecto.Migration

   def up do
    execute "create index idx_trgm_inode_parent on inodes using gin (parent gin_trgm_ops)"
  end

  def down do
    execute "drop index idx_trgm_inode_parent"
  end
end
