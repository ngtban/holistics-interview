defmodule RemoteTerminal.Repo.Migrations.CreateRootFolder do
  use Ecto.Migration

  def up do
    # id, parent, name, type, data, inserted_at, updated_at
    execute "insert into inodes values (nextval(pg_get_serial_sequence('inodes', 'id')), null, null, 'directory', null, now(), now())"
  end

  def down do
    execute "delete from inodes where parent is null and name is null"
  end
end
