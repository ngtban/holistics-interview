defmodule RemoteTerminal.Repo.Migrations.AddTrigramIndexForInodeNames do
  use Ecto.Migration

  def up do
    execute "create index idx_trgm_inode_name on inodes using gin (name gin_trgm_ops)"
  end

  def down do
    execute "drop index idx_trgm_inode_name"
  end
end
