# README
## Where is the write up?
Please check the file `WRITEUP.md` for the required write up.

## Setting up the project
Please follow the instructions in the subsections below to setup and get the project running.

### Prerequisites
Please install:
1. Elixir 1.10.04
2. Erlang 23
3. Postgres 13.4
4. Node v15.12.0

I cannot guarantee that the project would work with other Elixir or Postgres versions. Please make sure that you get the correct node version, as the project does not work with higher (and potentially lower) major node versions.

The project might work with lower or higher versions of Postgres as long as the version supports the `pg_trgm` extension.

### Getting the project up and running

#### Clone the repo to your local machine
1. Open a terminal
2. Navigate to a directory of your choice using`cd`
3. Run `git clone git@gitlab.com:ngtban/holistics-interview.git`
4. `cd` into the created directory

#### Installing dependencies

1. Run `mix deps.get` to fetch back-end dependencies.
2. Run `npm --prefix assets install` to fetch front-end dependencies.

#### Add your own database configuration
You will need to create database configuration files for `dev` and `test` environments which contains your own database name, host, etc.

So:
1. Create a `dev.local.exs` file within the `config` folder of the project
2. Create a database user with username and password matching the credentials used in the file above

For example, your `dev.local.exs` file should look like this:

```elixir
use Mix.Config

config :remote_terminal, RemoteTerminal.Repo,
  database: <your database name>,
  hostname: "localhost",
  username: <your username>,
  password: <your password>
```

Similarly, repeat the steps above but for the `test.local.exs` file.

As for creating the database user:
1. Open a terminal
2. Login as `postgresql` to get admin access
3. Run `psql` to open the psql cli
4. Run `create user <username> with password <password>;` with your username and password of choice.
5. Run `alter user <username> createdb;` to grant the user the right to create databases.
6. **Important** Make sure that you also grant the user with the `superuser` privilege, e.g `alter user <username> superuser;`. This is necessary as the app uses the `pg_trgm` extension to speed up queries.

#### Initializing the databases
Once you have followed the steps above, run these commands in a terminal
to let Rails setup the databases:

```bash
mix ecto.setup
mix ecto.migrate
```

Once the commands are finished the setup is complete.

#### Starting the server and checkout the terminal
To make sure that there are no issues, please:
1. Open a terminal
2. Start the Phoenix server with `iex -S mix phx.server`
3. Open a browser and navigate to `localhost:4000`.  You should see the terminal.
