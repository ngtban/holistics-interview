defmodule RemoteTerminalWeb.Router do
  use RemoteTerminalWeb, :router

  pipeline :browser do
    plug :accepts, ["html", "json"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", RemoteTerminalWeb do
    pipe_through :browser

    get "/", PageController, :index
    post "/execute", PageController, :execute
  end
end
