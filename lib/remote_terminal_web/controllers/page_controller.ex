defmodule RemoteTerminalWeb.PageController do
  use RemoteTerminalWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def execute(conn, params) do
    json(conn, Commands.Proxy.execute(params))
  end
end
