defmodule Commands.Create do
  def execute(%{"command" => command, "directory" => directory}) do
    [_command_name | options] = OptionParser.split(command)

    case parse_options(options) do
      {:error, message} ->
        %{
          output: "cr: #{message}",
          directory: directory
        }

      {:ok, {_, [], _}} ->
        %{
          output: "cr: missing path",
          directory: directory
        }

      {:ok, {options, [path], _}} ->
        recursive = get_in(options, [:path])

        wrap_create_for_output(directory, path, nil, recursive)

      {:ok, {options, [path, data], _}} ->
        recursive = get_in(options, [:path])

        wrap_create_for_output(directory, path, data, recursive)

      {:ok, {_, _, _}} ->
        %{
          output: "cr: too many arguments",
          directory: directory
        }
    end
  end

  def parse_options(options) do
    parsed_command = OptionParser.parse(options, strict: [path: :boolean], aliases: [p: :path])

    case parsed_command do
      {_, _, [{some_option, _} | _rest]} ->
        {:error, "invalid option -- '#{some_option}'"}

      {_, _, []} = result ->
        {:ok, result}
    end
  end

  def create_for(directory_path, file_path, data \\ "", recursive \\ false) do
    absolute_path = file_path |> Path.expand(directory_path)
    parent_path = absolute_path |> Path.dirname()
    file_name = Path.basename(absolute_path)

    cond do
      Commands.Utils.file_exist?(absolute_path) ->
        {:error, "file already exists: #{file_name}"}

      !recursive ->
        parent = Commands.Utils.ref_for(parent_path)

        inode_data = %{
          name: file_name,
          parent: parent,
          type: "file",
          data: data
        }

        inode_changeset = Schemas.Inode.upsert_changeset(inode_data)

        case Ecto.Changeset.apply_action(inode_changeset, :insert) do
          {:ok, data} ->
            RemoteTerminal.Repo.insert(data)

            {:ok, "file #{file_name} created"}

          {:error, changeset} ->
            if :parent in Keyword.keys(changeset.errors) do
              {:error,
               "cannot create file #{file_name}: Directory at #{parent_path} does not exist"}
            else
              [{field, {message, _source}} | _rest] = changeset.errors
              {:error, "validation failure: #{field} #{message}"}
            end
        end

      recursive ->
        {starting_path, remaining} =
          Commands.Utils.longest_existing_directory_path_from(parent_path)

        chaining = directory_create_chain_for(starting_path, remaining)

        with {:ok, multi, last_path} <- chaining,
             file_inode_data <- %{
               name: file_name,
               parent: Commands.Utils.ref_for(last_path),
               type: "file",
               data: data
             },
             file_inode_changeset <-
               Schemas.Inode.mass_crud_changeset(file_inode_data),
             {:ok, data} <- Ecto.Changeset.apply_action(file_inode_changeset, :insert) do
          multi
          |> Ecto.Multi.insert(file_path, data)
          |> RemoteTerminal.Repo.transaction()

          {:ok, "file at path #{file_path} created"}
        else
          {:error, changeset} ->
            [{field, {message, _source}}] = changeset.errors
            {:error, "validation failure: #{field} #{message}"}
        end
    end
  end

  def directory_create_chain_for(starting_path, remaining) do
    Enum.reduce_while(
      remaining,
      {:ok, Ecto.Multi.new(), starting_path},
      fn part, {:ok, multi, last_path} ->
        current_path = last_path |> Path.join(part)
        parent = Commands.Utils.ref_for(last_path)

        directory_inode_data = %{
          name: part,
          parent: parent,
          type: "directory",
          data: nil
        }

        directory_inode_changeset = Schemas.Inode.mass_crud_changeset(directory_inode_data)

        case Ecto.Changeset.apply_action(directory_inode_changeset, :insert) do
          {:ok, data} ->
            chained_multi = Ecto.Multi.insert(multi, current_path, data)
            {:cont, {:ok, chained_multi, current_path}}

          {:error, changeset} ->
            {:halt, {:error, changeset}}
        end
      end
    )
  end

  def wrap_create_for_output(directory, path, data \\ "", recursive \\ nil) do
    directory_path = Commands.Utils.get_path_for(directory)

    case create_for(directory_path, path, data, recursive) do
      {:ok, output} ->
        %{
          output: output,
          directory: directory
        }

      {:error, message} ->
        %{
          output: "cr: #{message}",
          directory: directory
        }
    end
  end
end
