defmodule Commands.Find do
  import Ecto.Query

  def execute(%{"command" => command, "directory" => directory}) do
    [_command_name | options] = OptionParser.split(command)

    case parse_options(options) do
      {:error, message} ->
        %{
          output: "find: #{message}",
          directory: directory
        }

      {:ok, {_, [name, path], _}} ->
        wrap_find_for_output(directory, name, path)

      {:ok, {_, [name], _}} ->
        wrap_find_for_output(directory, name)

      {:ok, {_, [], _}} ->
        %{
          output: "find: missing argument: name",
          directory: directory
        }
    end
  end

  def parse_options(options) do
    parsed_command = OptionParser.parse(options, strict: [])

    case parsed_command do
      {_, _, [{some_option, _} | _rest]} ->
        {:error, "invalid option -- '#{some_option}'"}

      {[], _, []} = result ->
        {:ok, result}
    end
  end

  def find_for(directory_path, name, path \\ "/") do
    search_path = path |> Path.expand(directory_path)

    query =
      Commands.Utils.all_descendants_of_query(search_path)
      |> where([i], like(i.name, ^"%#{name}%"))
      |> limit(10)
      |> select_merge(
        [i],
        %{
          parent: i.parent,
          name: i.name,
          created_at: i.inserted_at,
          type: i.type,
          size: fragment("char_length(coalesce(?, ''))", i.data)
        }
      )

    inodes = RemoteTerminal.Repo.all(query)

    output =
      case inodes do
        [_head | _rest] -> Commands.Utils.padded_listing_output(inodes, true)
        [] -> ""
      end

    {:ok, output}
  end

  def wrap_find_for_output(directory, name, path \\ "/") do
    directory_path = Commands.Utils.get_path_for(directory)

    case find_for(directory_path, name, path) do
      {:ok, output} ->
        %{
          output: output,
          directory: directory
        }

      {:error, message} ->
        %{
          output: "find: #{message}",
          directory: directory
        }
    end
  end
end
