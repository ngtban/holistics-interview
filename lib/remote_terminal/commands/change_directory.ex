defmodule Commands.ChangeDirectory do
  import Ecto.Query

  def execute(%{"command" => command, "directory" => directory}) do
    [_command_name | options] = OptionParser.split(command)

    case parse_options(options) do
      {:error, message} ->
        %{
          output: "cd: #{message}",
          directory: directory
        }

      {:ok, {_, [path], _}} ->
        wrap_change_directory_for_output(directory, path)

      {:ok, {_, [], _}} ->
        path = Commands.Utils.get_path_for(directory)

        wrap_change_directory_for_output(directory, path)
    end
  end

  def parse_options(options) do
    parsed_command = OptionParser.parse(options, strict: [])

    case parsed_command do
      {_, _, [{some_option, _} | _rest]} ->
        {:error, "invalid option -- '#{some_option}'"}

      {[], _, []} = result ->
        {:ok, result}
    end
  end

  def change_directory_for(directory_path, path) do
    absolute_path = path |> Path.expand(directory_path)

    directory_query =
      Commands.Utils.directory_at_query(absolute_path)
      |> select_merge([i], %{parent: i.parent, name: i.name})

    res = RemoteTerminal.Repo.all(directory_query)

    case res do
      [] ->
        {:error, "no such file or directory: #{path}"}

      [directory] ->
        {:ok, directory}
    end
  end

  def wrap_change_directory_for_output(directory, path) do
    directory_path = Commands.Utils.get_path_for(directory)

    case change_directory_for(directory_path, path) do
      {:ok, new_directory} ->
        %{
          output: "",
          directory: new_directory
        }

      {:error, message} ->
        %{
          output: "cd: #{message}",
          directory: directory
        }
    end
  end
end
