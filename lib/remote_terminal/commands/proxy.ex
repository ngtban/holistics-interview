defmodule Commands.Proxy do
  def execute(%{"command" => command, "directory" => directory} = options) do
    command_parts = OptionParser.split(command)

    case command_parts do
      ["cd" | _rest] ->
        Commands.ChangeDirectory.execute(options)

      ["cat" | _rest] ->
        Commands.Concatenate.execute(options)

      ["ls" | _rest] ->
        Commands.List.execute(options)

      ["cr" | _rest] ->
        Commands.Create.execute(options)

      ["find" | _rest] ->
        Commands.Find.execute(options)

      ["up" | _rest] ->
        Commands.Update.execute(options)

      ["mv" | _rest] ->
        Commands.Move.execute(options)

      ["man" | _rest] ->
        Commands.Manual.execute(options)

      [unknown_command | _rest] ->
        %{
          output: "unimplemented command: #{unknown_command}",
          directory: directory
        }
    end
  end
end
