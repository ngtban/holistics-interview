defmodule Commands.Utils do
  import Ecto.Query

  # Everything in here are meant to work with absolute paths

  def get_path_for(%{"parent" => parent, "name" => name}) do
    case parent do
      nil -> "/"
      "" -> "/#{name}"
      _ -> "#{parent}/#{name}"
    end
  end

  def directory_exist?(directory_path) do
    inode_exist?(directory_path, "directory")
  end

  def directory_at_query(directory_path) do
    inode_at_query(directory_path, "directory")
  end

  def file_exist?(file_path) do
    inode_exist?(file_path, "file")
  end

  def file_at_query(file_path) do
    inode_at_query(file_path, "file")
  end

  def inode_at_query(path, type \\ nil) do
    parent = path |> Path.dirname() |> ref_for
    name = path |> Path.basename()

    query =
      from(i in Schemas.Inode,
        select: %{id: i.id},
        limit: 1
      )

    root_aware_query =
      case path do
        "/" ->
          query
          |> where([i], is_nil(i.name) and is_nil(i.parent))

        _ ->
          query
          |> where([i], i.parent == ^parent and i.name == ^name)
      end

    maybe_typed_query =
      if type do
        root_aware_query
        |> where([i], i.type == ^type)
      else
        root_aware_query
      end

    maybe_typed_query
  end

  def inode_exist?(path, type \\ nil) do
    maybe_typed_query = inode_at_query(path, type)

    case RemoteTerminal.Repo.one(maybe_typed_query) do
      nil -> false
      _ -> true
    end
  end

  def ref_for(path) do
    case path do
      "/" -> ""
      _ -> path
    end
  end

  def longest_existing_directory_path_from(directory_path) do
    [root | path_parts] = Path.split(directory_path)

    ldp_helper(root, path_parts)
  end

  # Recursively query to find out the longest subpath with a corresponding directory
  # Eww
  def ldp_helper(last_path, [first | rest] = path_parts) do
    current_path = Path.join(last_path, first)

    if directory_exist?(current_path) do
      ldp_helper(current_path, rest)
    else
      {last_path, path_parts}
    end
  end

  def ldp_helper(last_path, []), do: {last_path, []}

  def all_descendants_of_query(path) do
    query =
      from(
        i in Schemas.Inode,
        select: %{id: i.id}
      )

    case path do
      "/" ->
        query

      _ ->
        query
        |> where([i], like(i.parent, ^"#{path}%"))
    end
  end

  # Having a 80-character width console in mind
  def padded_listing_output(inodes, full_path \\ false) do
    Enum.reduce(inodes, "", fn inode, acc ->
      path =
        if full_path do
          Commands.Utils.get_path_for(%{"name" => inode.name, "parent" => inode.parent})
        else
          inode.name
        end

      path_fragment = path |> String.pad_trailing(40)

      created_at_fragment =
        inode.created_at |> NaiveDateTime.to_string() |> String.pad_trailing(30)

      size_text =
        if inode.type == "directory" do
          "directory"
        else
          inode.size |> Integer.to_string()
        end

      size_fragment = size_text |> String.pad_trailing(10)

      "#{acc}#{path_fragment}#{created_at_fragment}#{size_fragment}\n"
    end)
  end

  # To see why this function uses Kernel.byte_size and Kernel.binary_part instead of slice,
  # check https://hexdocs.pm/elixir/1.12/String.html#module-string-and-binary-operations
  def take_prefix(full, prefix) do
    base = byte_size(prefix)

    binary_part(full, base, byte_size(full) - base)
  end
end
