defmodule Commands.Move do
  import Ecto.Query

  def execute(%{"command" => command, "directory" => directory}) do
    [_command_name | options] = OptionParser.split(command)

    case parse_options(options) do
      {:error, message} ->
        %{
          output: "up: #{message}",
          directory: directory
        }

      {:ok, {_, [], _}} ->
        %{
          output: "up: missing argument: path to file",
          directory: directory
        }

      {:ok, {_, [source_path, target_path], _}} ->
        wrap_move_for_output(directory, source_path, target_path)

      {:ok, {_, _, _}} ->
        %{
          output: "up: too many arguments"
        }
    end
  end

  def parse_options(options) do
    parsed_command = OptionParser.parse(options, strict: [])

    case parsed_command do
      {_, _, [{some_option, _} | _rest]} ->
        {:error, "invalid option -- '#{some_option}'"}

      {[], _, []} = result ->
        {:ok, result}
    end
  end

  def move_for(directory_path, source_path, target_path) do
    full_source_path = source_path |> Path.expand(directory_path)
    full_target_path = target_path |> Path.expand(directory_path)

    source_inode_query =
      Commands.Utils.inode_at_query(full_source_path)
      |> select_merge(
        [i],
        %{
          parent: i.parent,
          name: i.name,
          type: i.type,
          updated_at: fragment("now()")
        }
      )

    source_inode = RemoteTerminal.Repo.one(source_inode_query)

    target_inode_query =
      Commands.Utils.inode_at_query(full_target_path)
      |> select_merge(
        [i],
        %{
          parent: i.parent,
          name: i.name,
          type: i.type,
          updated_at: fragment("now()")
        }
      )

    target_inode = RemoteTerminal.Repo.one(target_inode_query)

    cond do
      source_path == "/" ->
        {:error, "cannot move root folder"}

      source_path == "." or source_path == ".." ->
        {:error, "refuse to work with either . or .. alias"}

      source_inode.type == "directory" and
        target_inode.type == "directory" and
          String.starts_with?(source_path, full_target_path) ->
        {:error, "cannot move '#{source_path}' to a subdirectory of itself, '#{target_path}'"}

      is_nil(source_inode) ->
        {:error, "cannot access #{source_path}: No such file or directory"}

      is_nil(target_inode) ->
        {:error, "cannot access #{target_path}: No such directory"}

      source_inode.type == "file" ->
        Commands.MovementService.move_file(source_inode, full_target_path)

      source_inode.type == "directory" ->
        Commands.MovementService.move_directory(source_inode, full_target_path)
    end
  end

  def wrap_move_for_output(directory, source_path, target_path) do
    directory_path = Commands.Utils.get_path_for(directory)

    case move_for(directory_path, source_path, target_path) do
      {:ok, output} ->
        %{
          output: output,
          directory: directory
        }

      {:error, message} ->
        %{
          output: "up: #{message}",
          directory: directory
        }
    end
  end
end
