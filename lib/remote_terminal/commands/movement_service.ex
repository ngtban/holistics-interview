defmodule Commands.MovementService do
  import Ecto.Query

  def move_file(file_inode, target_path, new_name \\ nil, data \\ nil) do
    changed_name = new_name || file_inode.name

    file_data_changes = %{
      name: changed_name,
      parent: target_path
    }

    updated_file_data = Map.merge(file_inode, file_data_changes)

    updated_file_data =
      if not is_nil(data) do
        Map.merge(updated_file_data, %{data: data})
      else
        updated_file_data
      end

    updated_file_changeset =
      Schemas.Inode.update_changeset(
        %Schemas.Inode{id: file_inode.id},
        updated_file_data
      )

    if updated_file_changeset.valid? do
      RemoteTerminal.Repo.update(updated_file_changeset)

      {:ok, ""}
    else
      file_path =
        Commands.Utils.get_path_for(%{
          "parent" => file_inode.parent,
          "name" => file_inode.name
        })

      [{field, {message, _source}}] = updated_file_changeset.errors

      {:error,
       "file validation failed: #{field} #{message} when moving #{file_path} into #{target_path}"}
    end
  end

  def move_directory(directory_inode, target_path, new_name \\ nil) do
    directory_path =
      Commands.Utils.get_path_for(%{
        "parent" => directory_inode.parent,
        "name" => directory_inode.name
      })

    changed_name = new_name || directory_inode.name

    multi =
      Ecto.Multi.new()
      |> Ecto.Multi.run(:updated_directory_changeset, fn _repo, _changes ->
        directory_changeset =
          Schemas.Inode.update_changeset(
            %Schemas.Inode{id: directory_inode.id},
            Map.merge(directory_inode, %{name: changed_name, parent: target_path})
          )

        if directory_changeset.valid? do
          {:ok, directory_changeset}
        else
          {:error, directory_changeset}
        end
      end)
      |> Ecto.Multi.update(:update_directory_name, fn changes ->
        changes.updated_directory_changeset
      end)
      |> Ecto.Multi.run(:list_inode_data, fn _repo, _changes ->
        descendants_query =
          Commands.Utils.all_descendants_of_query(directory_path)
          |> select_merge(
            [i],
            %{
              parent: i.parent,
              name: i.name,
              type: i.type,
              updated_at: fragment("now()"),
              inserted_at: i.inserted_at
            }
          )

        descendant_inodes = RemoteTerminal.Repo.all(descendants_query)

        new_directory_path = Path.join(directory_inode.parent, changed_name)

        updated_parent_inodes_for(descendant_inodes, directory_path, new_directory_path)
      end)
      |> Ecto.Multi.insert_all(
        :insert_list_inode_data,
        Schemas.Inode,
        fn changes ->
          changes.list_inode_data
        end,
        on_conflict: {:replace, [:parent, :updated_at]},
        conflict_target: [:id]
      )

    multi_result = RemoteTerminal.Repo.transaction(multi)

    case multi_result do
      {:ok, _changes} ->
        {:ok, ""}

      {:error, :list_inode_data, changeset, _changes} ->
        failed_inode_path =
          Commands.Utils.get_path_for(%{
            "parent" => changeset.parent,
            "name" => changeset.name
          })

        [{field, {message, _source}} | _rest] = changeset.errors
        {:error, "validation failed: #{field} #{message} for #{failed_inode_path}"}

      {:error, :insert_list_inode_data, _failed_value, _changes} ->
        {:error, "failed to move descendant files and directories"}
    end
  end

  def updated_parent_inodes_for(inodes, source_path, target_path) do
    Enum.reduce_while(inodes, {:ok, []}, fn inode, {:ok, list_inode_data} ->
      chopped_inode_parent_path = Commands.Utils.take_prefix(inode.parent, source_path)

      new_inode_parent_path = Path.join(target_path, chopped_inode_parent_path)

      updated_inode_data = Map.merge(inode, %{parent: new_inode_parent_path})

      inode_changeset = Schemas.Inode.mass_crud_changeset(updated_inode_data)

      case Ecto.Changeset.apply_action(inode_changeset, :insert) do
        {:ok, data} ->
          validated_inode_data = Map.take(data, [:id, :parent, :type, :updated_at, :inserted_at])

          {:cont, {:ok, [validated_inode_data | list_inode_data]}}

        {:error, changeset} ->
          {:halt, {:error, changeset}}
      end
    end)
  end
end
