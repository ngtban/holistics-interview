defmodule Commands.List do
  import Ecto.Query, only: [from: 2]

  def execute(%{"command" => command, "directory" => directory}) do
    [_command_name | options] = OptionParser.split(command)

    case parse_options(options) do
      {:error, message} ->
        %{
          output: "ls: #{message}",
          directory: directory
        }

      {:ok, {_, [path], _}} ->
        wrap_list_for_output(directory, path)

      {:ok, {_, [], _}} ->
        path = Commands.Utils.get_path_for(directory)

        wrap_list_for_output(directory, path)
    end
  end

  def parse_options(options) do
    parsed_command = OptionParser.parse(options, strict: [])

    case parsed_command do
      {_, _, [{some_option, _} | _rest]} ->
        {:error, "invalid option -- '#{some_option}'"}

      {[], _, []} = result ->
        {:ok, result}
    end
  end

  def list_for(directory_path, path) do
    absolute_path = path |> Path.expand(directory_path)

    parent = Commands.Utils.ref_for(absolute_path)

    cond do
      Commands.Utils.directory_exist?(absolute_path) ->
        inodes =
          RemoteTerminal.Repo.all(
            from(i in Schemas.Inode,
              where: i.parent == ^"#{parent}",
              select: %{
                name: i.name,
                created_at: i.inserted_at,
                type: i.type,
                size: fragment("char_length(coalesce(?, ''))", i.data)
              }
            )
          )

        output = Commands.Utils.padded_listing_output(inodes)

        {:ok, output}

      Commands.Utils.file_exist?(absolute_path) ->
        {:ok, "#{path}"}

      true ->
        {:error, "cannot access '#{path}': No such file or directory"}
    end
  end

  def wrap_list_for_output(directory, path) do
    directory_path = Commands.Utils.get_path_for(directory)

    case list_for(directory_path, path) do
      {:ok, output} ->
        %{
          output: output,
          directory: directory
        }

      {:error, message} ->
        %{
          output: "ls: #{message}",
          directory: directory
        }
    end
  end
end
