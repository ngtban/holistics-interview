defmodule Commands.Update do
  import Ecto.Query

  def execute(%{"command" => command, "directory" => directory}) do
    [_command_name | options] = OptionParser.split(command)

    case parse_options(options) do
      {:error, message} ->
        %{
          output: "up: #{message}",
          directory: directory
        }

      {:ok, {_, [], _}} ->
        %{
          output: "up: missing argument: path to file",
          directory: directory
        }

      {:ok, {_, [path, name], _}} ->
        wrap_update_for_output(directory, path, name)

      {:ok, {_, [path, name, data], _}} ->
        wrap_update_for_output(directory, path, name, data)

      {:ok, {_, _, _}} ->
        %{
          output: "up: too many arguments"
        }
    end
  end

  def parse_options(options) do
    parsed_command = OptionParser.parse(options, strict: [])

    case parsed_command do
      {_, _, [{some_option, _} | _rest]} ->
        {:error, "invalid option -- '#{some_option}'"}

      {[], _, []} = result ->
        {:ok, result}
    end
  end

  def update_for(directory_path, path, name, data \\ nil) do
    inode_path = path |> Path.expand(directory_path)

    source_inode_query =
      Commands.Utils.inode_at_query(inode_path)
      |> select_merge(
        [i],
        %{
          parent: i.parent,
          name: i.name,
          type: i.type,
          updated_at: fragment("now()")
        }
      )

    source_inode = RemoteTerminal.Repo.one(source_inode_query)

    cond do
      path == "/" ->
        {:error, "cannot rename root folder"}

      path == "." or path == ".." ->
        {:error, "refuse to work with either . or .. alias"}

      is_nil(source_inode) ->
        {:error, "cannot access #{path}: No such file or directory"}

      source_inode.type == "file" ->
        Commands.MovementService.move_file(source_inode, source_inode.parent, name, data)

      source_inode.type == "directory" and not is_nil(data) ->
        {:error, "updating data for a directory is not possible"}

      source_inode.type == "directory" ->
        Commands.MovementService.move_directory(source_inode, source_inode.parent, name)
    end
  end

  def wrap_update_for_output(directory, path, name, data \\ nil) do
    directory_path = Commands.Utils.get_path_for(directory)

    case update_for(directory_path, path, name, data) do
      {:ok, output} ->
        %{
          output: output,
          directory: directory
        }

      {:error, message} ->
        %{
          output: "up: #{message}",
          directory: directory
        }
    end
  end
end
