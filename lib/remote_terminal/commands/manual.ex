defmodule Commands.Manual do
  def execute(%{"command" => command, "directory" => directory}) do
    [_command_name | options] = OptionParser.split(command)

    case parse_options(options) do
      {:error, message} ->
        %{
          output: "man: #{message}",
          directory: directory
        }

      {:ok, {_, [command], _}} ->
        wrap_manual_for_output(directory, command)

      {:ok, {_, [], _}} ->
        %{
          output: "What manual page do you want?\nFor example, try 'man ls'.",
          directory: directory
        }
    end
  end

  def parse_options(options) do
    parsed_command = OptionParser.parse(options, strict: [])

    case parsed_command do
      {_, _, [{some_option, _} | _rest]} ->
        {:error, "invalid option -- '#{some_option}'"}

      {[], _, []} = result ->
        {:ok, result}
    end
  end

  def manual_for(command) do
    output =
      case command do
        "cd" ->
          "cd(1)
          name
            cd - change current working directory/folder
          synopsis
            CD FOLDER_PATH
          description:
            change current working directory/folder to the specified FOLDER_PATH"

        "cr" ->
          "cr
          name
            cr - create a new file
          synopsis
            cr [-p] PATH [DATA]
          description:
            Create a file at the given PATH and store DATA in the file
          options:
            -p create all folders in the path if they do not exist"

        "cat" ->
          "cat(1)
          name
            cat - list directory contents
          synopsis
            cat FILE_PATH
          description:
            show the content of a file at FILE_PATH"

        "ls" ->
          "ls(1)
          name
            ls - list directory contents
          synopsis
            ls [FOLDER_PATH]
          description:
            list out all items directly under a folder"

        "find" ->
          "find(1)
          name
            find - search all files/folders
          synopsis
            find NAME [FOLDER_PATH]
          description:
            search all files/folders whose name contains the substring NAME"

        "up" ->
          "up(1)
          name
            up - rename file/folder
          synopsis
            up PATH NAME [DATA]
          description:
            rename the file/folder to NAME and update the data to DATA if the target is a file"

        "mv" ->
          "mv(1)
          name
            mv - move a file/folder
          synopsis
            mv PATH FOLDER_PATH
          description:
            move the file/folder at PATH into FOLDER_PATH"

        "clear" ->
          "clear(1)
          name
            clear - clear terminal output
          synopsis
            clear
          description:
            empty the terminal"

        _ ->
          "No manual entry for #{command}"
      end

    {:ok, output}
  end

  def wrap_manual_for_output(directory, command) do
    case manual_for(command) do
      {:ok, output} ->
        %{
          output: output,
          directory: directory
        }

      {:error, message} ->
        %{
          output: "man: #{message}",
          directory: directory
        }
    end
  end
end
