defmodule Commands.Concatenate do
  import Ecto.Query

  def execute(%{"command" => command, "directory" => directory}) do
    [_command_name | options] = OptionParser.split(command)

    case parse_options(options) do
      {:error, message} ->
        %{
          output: "cat: #{message}",
          directory: directory
        }

      {:ok, {_, [path], _}} ->
        wrap_concatenate_for_output(directory, path)

      {:ok, {_, [], _}} ->
        %{
          output: "cat: missing argument: path to file",
          directory: directory
        }
    end
  end

  def parse_options(options) do
    parsed_command = OptionParser.parse(options, strict: [])

    case parsed_command do
      {_, _, [{some_option, _} | _rest]} ->
        {:error, "invalid option -- '#{some_option}'"}

      {[], _, []} = result ->
        {:ok, result}
    end
  end

  def list_for(directory_path, path) do
    absolute_path = path |> Path.expand(directory_path)

    file_query =
      Commands.Utils.file_at_query(absolute_path)
      |> select_merge(
        [i],
        %{
          data_length: fragment("char_length(?)", i.data),
          truncated_data: fragment("substring(? for 1000)", i.data)
        }
      )

    file = RemoteTerminal.Repo.all(file_query)

    case file do
      [file] ->
        data =
          if file.data_length > 1000 do
            "#{file.truncated_data}\n(content truncated)"
          else
            "#{file.truncated_data}"
          end

        {:ok, data}

      [] ->
        {:error, "cannot access '#{path}': No such file or directory"}
    end
  end

  def wrap_concatenate_for_output(directory, path) do
    directory_path = Commands.Utils.get_path_for(directory)

    case list_for(directory_path, path) do
      {:ok, output} ->
        %{
          output: output,
          directory: directory
        }

      {:error, message} ->
        %{
          output: "cat: #{message}",
          directory: directory
        }
    end
  end
end
