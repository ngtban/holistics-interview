defmodule Schemas.Inode do
  use Ecto.Schema
  import Ecto.Changeset

  schema "inodes" do
    field(:parent, :string, default: "")
    field(:name, :string)
    field(:type, :string)
    field(:data, :string)

    timestamps()
  end

  @required_fields ~w(parent name type)a
  @valid_inode_types ~w(file directory)

  def update_changeset(inode, attributes) do
    inode
    |> cast(attributes, __MODULE__.__schema__(:fields), empty_values: [""])
    |> validate()
    |> validate_parent_exists()
  end

  def upsert_changeset(attributes) do
    %Schemas.Inode{}
    |> cast(attributes, __MODULE__.__schema__(:fields), empty_values: [""])
    |> validate()
    |> validate_parent_exists()
  end

  def mass_crud_changeset(attributes) do
    %Schemas.Inode{}
    |> cast(attributes, __MODULE__.__schema__(:fields), empty_values: [""])
    |> validate()
  end

  def validate(inode) do
    inode
    |> validate_not_nil(@required_fields)
    |> validate_inclusion(:type, @valid_inode_types)
    |> unique_constraint([:parent, :name])
    |> validate_change(:name, fn :name, name ->
      valid_name = String.match?(name, ~r/^[a-zA-Z0-9 _-]+$/m)

      if get_field(inode, :parent) != "" and !valid_name do
        [:name, "contains invalid characters"]
      else
        []
      end
    end)
  end

  def validate_not_nil(changeset, fields) do
    Enum.reduce(fields, changeset, fn field, changeset ->
      if get_field(changeset, field) == nil do
        add_error(changeset, field, "is nil")
      else
        changeset
      end
    end)
  end

  def validate_parent_exists(changeset) do
    changeset
    |> validate_change(:parent, fn :parent, parent ->
      if Commands.Utils.directory_exist?(parent) do
        []
      else
        [parent: "does not exist"]
      end
    end)
  end
end
