defmodule RemoteTerminal.Repo do
  use Ecto.Repo,
    otp_app: :remote_terminal,
    adapter: Ecto.Adapters.Postgres
end
