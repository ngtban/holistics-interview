# Technical Decisions and Other Things

## Back-end
### Using Elixir, Phoenix to build the back-end
I chose to use Elixir and Phoenix because:
1. Phoenix is ran on top of the Erlang VM, which in turns is well known for how easy it is to build a fault-tolerant, scalable system on top.
2. I really like Elixir's elegant syntax, pattern matching, how it treats operations as modifying data on a pipeline rather than doing things in an object-centric way.
3. I worked with Ecto and found it to be really great for building complex query, especially how its syntax matches SQL closely, and the benefits of having a type-checking system to prevent malformed queries.
4. Ecto also provides a great way to structure database operations, which is `Ecto.Multi`.  In essence, it is very much like JS promises in how units of work could be chained together and how people could "hook" into the outputs of each and handle errors, etc.

### Problems with modelling hierarchical data using a relational database
Inspired by Linux inode system, I modelled both file and folders using the same table named `inodes`.

I am aware of the problems with using adjacency lists to model hierarchical data, so I opted to store the full path to the parent in the `parent` field for each inode instead. Searching for descendants are done by using a `like` prefix-matching query.

Operations that involve searching for all descendants like `mv` or `up` should theoretically be faster as long as the appropriate indices exist on `parent`.

### Speeding up descendant searches and by-name searches
To speed up searches involving path prefixes, I used Postgres' `pg_trgm` extension to add a trigram index on `parent`.

Trigram indices in the long run could be "poisoned" in the sense there are plenty of search results sharing the same prefix, which then requires ranking of the results by relevancy - a costly operation.

I tried using GIN index to solve the prefix searching problem at first, using a query like `to_tsquery('simple', prefix || ':*') @@ ts_vector(parent)`, but I am not sure how this would perform. There are very few articles on the internet touching this topic, apart from Postgres' own documentation, which unfortunately does not address performance issues using the `ts_query` prefix-matching operator. In the end, I switched to using trigram indices, because that seems like a well-worn path.

In retrospect, the best way to determine the performance is to do benchmarks, which I did not have the time to :D.

## Front-end

### Finding a library that emulates a terminal and works with React
Unfortunately all the libraries that I could find that emulates a terminal and works as a React component are not great. So I simply picked `react-console-emulator`, [link to GitHub](https://github.com/linuswillner/react-console-emulator).

The component simply receives a set of objects describing commands and the function the be ran whenever that command is invoked, something like this:
```js
{
	[command_name]: {
		fn: function_handling_the_command
	}
}
```

In the case that the function is `async` the library simply awaits the function. There are no other options to manipulate the printed output, which is not great for what I wanted to do. If given time, I probably would try to write my own.

### Theming
I unfortunately am rather terrible at making the front-end side looks pretty. And I also did not have the time :D. Implementing the commands on the back-end took quite a bit of time.

Currently I simply added some CSS styling to the app's background, title, etc. to make it looks less jarring. No CSS-in-JS or anything fancy like that. A future improvement could introduce CSS-in-JSS, as well as theme customization.

The default terminal also looks atrocious :D.

## Some potential improvements
This app would benefit from:
1. A rate limiter, which prevents malicious users from programmatically flooding the server with requests. The library [ExRated](https://hex.pm/packages/ex_rated) looks promising, but unforunately I did not have the time to implement this functionality.
2. More thorough look at concurrent operations and how to avoid issues around them. Transactions and such.
3. Command auto-complete, by pressing `tab` for example.
4. Theme customization

And that is all from me.
